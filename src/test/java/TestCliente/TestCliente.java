/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestCliente;

import ec.st.mecanicasanchez.modelo.Cliente;
import ec.st.mecanicasanchez.modelo.ClienteDAO;
import ec.st.mecanicasanchez.modelo.Conexion;
import ec.st.mecanicasanchez.modelo.MockResultTest;
import exepciones.ClienteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author jhander
 */
public class TestCliente {

    private Conexion conexion;
    @Mock
    private Connection con;
    @Mock
    private PreparedStatement ps;
    @Mock
    private ResultSet rs;
    @Mock
    private MockResultTest mrs;
    private ClienteDAO cliente;
    @Mock
    List<Cliente> clientes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        conexion = Mockito.mock(Conexion.class, Mockito.RETURNS_DEEP_STUBS);
        cliente = Mockito.spy(new ClienteDAO(conexion));

        con = Mockito.mock(Connection.class, Mockito.RETURNS_DEEP_STUBS);
        rs = Mockito.mock(ResultSet.class, Mockito.RETURNS_DEEP_STUBS);
        clientes = Mockito.mock(ArrayList.class);
    }

    /**
     * Ejecucion normal del metodo agregar de la clase ClienteDAO
     *
     * @throws Exception
     */
    @Test
    public void agregarClienteNormal() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("Luis");
        c.setCelular(Integer.parseInt("0999999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.agregar(c));
        Assert.assertEquals(1, cliente.agregar(c));
    }
//

    /**
     * Escenario que representa cuando se ingresa un cliente nulo, lo cual no se
     * debe permitir
     *
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void agregarClienteNull() throws Exception {
        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.agregar(null));
        Assert.assertEquals(0, cliente.agregar(null));
    }

    /**
     * Escenario que represesenta cuando se intenta enviar datos no validos como
     * es un nombre del cliente en donde no se contenga el apellido para la
     * identificacion del mismo
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void agregarClienteDatoNoValido() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("a");
        c.setCelular(Integer.parseInt("0999999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.agregar(c));
        Assert.assertEquals(1, cliente.agregar(c));
    }

    /**
     * Escenario que represesenta cuando se intenta enviar datos no validos como
     * un id de vehiculo inexiste o numero de celular incorrectos o nombre del
     * cliente vacio o nulos
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void agregarClienteDatosNulos() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(0);
        c.setNombre(null);
        c.setCelular(0);

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.agregar(c));
        Assert.assertEquals(0, cliente.agregar(c));
    }

    /**
     * Escenario que representa si algun empleado intenta agregar un cliente sin
     * los datos necesarios o campos vacios como el nombre
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void agregarClienteDatoNombreVacio() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("");
        c.setCelular(Integer.parseInt("0999999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.agregar(c));
        Assert.assertEquals(0, cliente.agregar(c));
    }

    /**
     * Ejecucion normal del metodo actualizar de la clase ClienteDAO
     *
     * @throws Exception
     */
    @Test
    public void actualizarClienteNormal() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("Juan");
        c.setCelular(Integer.parseInt("0999999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.actualizar(c));
        Assert.assertEquals(0, cliente.actualizar(c));
    }

    /**
     * Escenario que representa cuando se intenta actualizar un cliente nulo, lo
     * cual no se debe permitir
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void actualizarClienteNull() throws Exception {
        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.actualizar(null));
        Assert.assertEquals(1, cliente.actualizar(null));
    }

    /**
     * Metodo e escenario que indica lo sucedido en caso de que se intente
     * enviar datos del cliente vacios o no validos
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void actualizarClienteDatosNulos() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(0);
        c.setNombre(null);
        c.setCelular(0);

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.actualizar(c));
        Assert.assertEquals(1, cliente.actualizar(c));
    }

    /**
     * Escenario en donde al intentar actualizar un cliente el nombre del mismo
     * no es permitido por falta de informacion como apellido
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void actualizarClienteDatosNoValido() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("A");
        c.setCelular(Integer.parseInt("0999999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.actualizar(c));
        Assert.assertEquals(1, cliente.actualizar(c));
    }

    /**
     * Escenario en el que se intenta enviar campos vacios requeridos como el
     * nombre del cliente
     *
     * @throws Exception
     */
    @Test(expected = ClienteException.class)
    public void actualizarClienteDatoVacio() throws Exception {
        Cliente c = new Cliente();
        c.setCarro_id(1);
        c.setNombre("");
        c.setCelular(Integer.parseInt("099999999"));

        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.actualizar(c));
        Assert.assertEquals(1, cliente.actualizar(c));
    }

//    @Test
//    public void buscarClienteNormal() throws Exception {
//        Cliente c = new Cliente();
//        c.setCarro_id(1);
//        c.setNombre("Test");
//        c.setCelular(Integer.parseInt("0987654321"));
//        clientes.add(c);
//
//        Mockito.when(conexion.getConnection()).thenReturn(con);
//        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
//        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
//        ps.setString(1, c.getNombre());
//        Mockito.when(ps.executeQuery()).thenReturn(rs);
//        Assert.assertEquals(rs.next(), false);
//    }
    /**
     * Ejecucion normal del metodo buscar de la clase ClienteDAO
     *
     * @throws Exception
     */
    @Test(expected = NullPointerException.class)
    public void buscarClienteNormal() throws Exception {
        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.buscar("POW-134"));
        Assert.assertEquals(clientes, cliente.buscar("POW134"));
    }
    
    /**
     * Escenario representando en caso de que se intente enviar un dato nulo para
     * la busqueda
     * 
     * @throws Exception 
     */
    @Test(expected = Exception.class)
    public void buscarClienteNull() throws Exception {
        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.buscar(null));
        Assert.assertEquals(clientes, cliente.buscar(null));
    }
    
    /**
     * Escenario representando en caso de que se intente enviar un dato vacio para
     * la busqueda
     * 
     * @throws Exception 
     */
    @Test(expected = ClienteException.class)
    public void buscarClienteDatosVacio() throws Exception {
        Mockito.when(conexion.getConnection()).thenReturn(con);
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        Mockito.when(conexion.getConnection().prepareStatement(sql)).thenReturn(ps);
        System.out.println("Response Client " + cliente.buscar(""));
        Assert.assertEquals(clientes, cliente.buscar(""));
    }

}
