/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package TestMantenimiento;

import ec.st.mecanicasanchez.modelo.Conexion;
import ec.st.mecanicasanchez.modelo.Mantenimiento;
import ec.st.mecanicasanchez.modelo.MantenimientoDAO;
import ec.st.mecanicasanchez.modelo.MockResultTest;
import ec.st.mecanicasanchez.modelo.Vehiculo;
import exepciones.MantenimientoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Santiago Tuqueres
 */
public class TestDAOMantenimiento {
    //mock hace un objeto cascaron
    //spy hace un objeto cascaron pero se puede acceder alos metodos
    //en forma de simulacion

    private Conexion conexion;
    @Mock
    private Connection mockConnection;
    @Mock
    private PreparedStatement mockPreparedStatement;
    @Mock
    private ResultSet rs;
    @Mock
    private MockResultTest mrs;
    private MantenimientoDAO mantenimiento;
    @Mock
    List<Vehiculo> datos;

    //
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        conexion = Mockito.mock(Conexion.class, Mockito.RETURNS_DEEP_STUBS);
        mantenimiento = Mockito.spy(new MantenimientoDAO(conexion));
        mockConnection = Mockito.mock(Connection.class, Mockito.RETURNS_DEEP_STUBS);
        rs = Mockito.mock(ResultSet.class, Mockito.RETURNS_DEEP_STUBS);
        datos = Mockito.mock(ArrayList.class);
    }

    /**
     * El presente escenario representa la ejecucion normal del metodo crear
     * Mantenimiento
     *
     * @throws Exception
     */
    @Test
    public void crearMantenimientoNormalTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("07-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + mantenimiento.agregar(m));

    }

    /**
     * El presente escenario representa cuando se quiere ingresar un valor de
     * kilometraje negativo ya que en una sotuacion real el kilometraje nunca va
     * a ser negativo sino q su valor tiende de cero en adelante
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void crearMantenimientokilometrajeNegativoTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("07-12-2021");
        m.setKilometraje(-1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento2" + mantenimiento.agregar(m));

    }

    /**
     * El presente escenario representa cuando se quiere ingresar un valor de id
     * negativo ya que en una sotuacion real el id que identifica a dicho
     * mantenimiento nunca va a ser negativo
     *
     * @throws Exception
     */
    @Test
    public void crearMantenimientoNegativoIDTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(-1);
        m.setFecha_revision("07-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento2" + mantenimiento.agregar(m));

    }

    /**
     * El presente escenario representa cuando no se ingresa una fecha ára el
     * registro de un mantenimiento, lo cual no deberia permitirse ya que dicho
     * atributo es muy indispensable en la vida real para tener seguimiento a un
     * determinado vehiculo
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void crearMantenimientoSinFechaTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento2" + mantenimiento.agregar(m));

    }

    /**
     * El presente escenario representa cuando al ingresar una observacion para
     * un determinado mantenimiento se envia un valor null, lo cual no esta
     * permitido pero si se permite envoar un String vacio, ya que no siempre
     * existen observaciones que se deban registrar
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void crearMantenimientoSinObservacionesTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("10-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones(null);
        m.setServicio("Servicios ....");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento2" + mantenimiento.agregar(m));

    }

    /**
     * El presente escenario representa cuando al ingresar los servicios
     * realizados a un determinado mantenimiento se envia un valor null o no se
     * registran, en dicho caso se abprta la operacion ya que de manera
     * obligatoria se debe ingresar algun servicio, por tanto este valor no debe
     * ser vacio no ser nulo. que se deban registrar
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void crearMantenimientoSinServicioTest() throws Exception {
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("10-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("sss");
        m.setServicio("");

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento2" + mantenimiento.agregar(m));

    }
//Pruebas Buscar

    /**
     * El presente escenario representa cuando al ingresar una placa para buscar
     * un determinado vehiculo se ingresa un texto vacio, lo cual no se debe
     * permitir para evitar que por algun error del usuario suceda este caso.
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void buscarMantenimientoSinPlaca() throws Exception {

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + mantenimiento.buscar(""));

    }

    /**
     * El presente escenario representa cuando al ingresar una placa para buscar
     * un determinado vehiculo se ingresa un valor nulo, lo cual no se debe
     * permitir para evitar que por algun error del usuario suceda este caso.
     *
     * @throws Exception
     */
    @Test(expected = MantenimientoException.class)
    public void buscarMantenimientoPlacaNull() throws Exception {

        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String query = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        when(mockConnection.prepareStatement(query)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + mantenimiento.buscar(null));

    }
    
    ///////ELIMINAR
    /**
     * El presente escenario representa el flujo normal cuando se va a elminar un mantenimiento previamente registrado.
     * Previamente se crea el objeto mantenimiento, se le asigan sus datos correspondientes, posteriormente se realiza la busqueda mediante el id del mantenimiento y se precoede a eliminar dicho mantenimiento
     *
     * @throws Exception
     */
    @Test
    public void ElminarMentenimientoNormalTest  () throws Exception{
        int id = 1;
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("07-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");
        System.out.println("hi");
        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String sql = "delete from mantenimiento where mantenimiento_id=" + id;
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
        
        System.out.println("Mantenimiento eliminado");
    }
    
    /**
     * El presente escenario representa cuando se va a elminar un mantenimiento pero el no existe dicho objeto con el id ingresado.
     * Previamente se crea el objeto mantenimiento, se le asigan sus datos correspondientes, posteriormente se realiza la busqueda mediante el id (creado anteriormente) que no existir[ia en la bd.
     *
     * @throws Exception
     */
        @Test //ESCENARIO CUANDO DESEEE ELMINAR UNA MANTENIMIENTO PERO ESTE NO EXISTA
    public void TestElminarMentenimientoNoExisteID  () throws Exception{
        int id = 4;
        Mantenimiento m = new Mantenimiento();
        m.setCarro_id(1);
        m.setFecha_revision("07-12-2021");
        m.setKilometraje(1000);
        m.setMantenimiento_id(1);
        m.setObservaciones("se observo q ...");
        m.setServicio("Servicios ....");
        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String sql = "delete from mantenimiento where mantenimiento_id=" + id;
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
        System.out.println("No existe la ID");
    }
     /////BUSCAR
    /**
     * El presente escenario representa el flujo normal cuando se va a realizar la busqueda de un mantenimiento por su la placa del vehiculo que fue registrado el mantenimientoi.
     * Para esto se debe ingresar la placa del vehiculo existente
     *
     * @throws Exception
     */
     @Test//ESCENARIO AL BUSCAR MANTENIMIENTO POR PLACA
    public void BuscarMantenimientoTest() throws Exception {
          Vehiculo v = new Vehiculo();
        v.setPlaca("laa");
        v.setColor("negro");
//        List L<Vehiculo> datos = new ArrayList<>();
        datos.add(v);
        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String sql = "select mantenimiento_id, fecha, kilometraje, servicio, observaciones from mantenimiento m INNER JOIN vehiculo v  ON m.carro_id = v.carro_id  where v.placa LIKE ?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
        mockPreparedStatement.setString(1, v.getPlaca());
        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        assertEquals(rs.next(), false);
    }
    
    /**
     * El presente escenario representa cuando se va a realizar la busqueda de un mantenimiento por su la placa del vehiculo
     * pero no se envian datos para realizar dicha busqueda
     * Para esto se debe ingresar la placa del vehiculo existente
     *
     * @throws Exception
     */
     @Test//ESCENARIO AL BUSCAR MANTENIMIENTO POR PLACA NO SE INGRESAN DATOS
    public void BuscarMantenimientoNullTest() throws Exception {
          Vehiculo v = new Vehiculo();
        v.setPlaca("laa");
        v.setColor("negro");
//        List L<Vehiculo> datos = new ArrayList<>();
        datos.add(v);
        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String sql = "select mantenimiento_id, fecha, kilometraje, servicio, observaciones from mantenimiento m INNER JOIN vehiculo v  ON m.carro_id = v.carro_id  where v.placa LIKE ?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
        mockPreparedStatement.setString(1, "");
        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        assertEquals(rs.next(), false);
         System.out.println("Se debe ingresar una placa");
    }
    
    /**
     * El presente escenario representa el flujo normal cuando se va a realizar la busqueda de un mantenimiento por la placa del vehiculo.
     * En este caso se esta ingresando una placa la cual no xiste en la BD
     *
     * @throws Exception
     */
      @Test//ESCENARIO AL BUSCAR MANTENIMIENTO POR PLACA, NO EXISTE DICHA PLACA
    public void BuscarMantenimientoTestNoExistePLaca() throws Exception {
          Vehiculo v = new Vehiculo();
        v.setPlaca("laa");
        v.setColor("negro");
//        List L<Vehiculo> datos = new ArrayList<>();
        datos.add(v);
        Mockito.when(mantenimiento.getConn()).thenReturn(mockConnection);
        String sql = "select mantenimiento_id, fecha, kilometraje, servicio, observaciones from mantenimiento m INNER JOIN vehiculo v  ON m.carro_id = v.carro_id  where v.placa LIKE ?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
        mockPreparedStatement.setString(1, "POW134");
        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        assertEquals(rs.next(), false);
        System.out.println(" No existe esa placa");
    }
}
