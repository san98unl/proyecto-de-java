/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestVehiculo;

import ec.st.mecanicasanchez.modelo.Conexion;
import ec.st.mecanicasanchez.modelo.MockResultTest;
import ec.st.mecanicasanchez.modelo.Vehiculo;
import ec.st.mecanicasanchez.modelo.VehiculoDAO;
import exepciones.VehiculoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author EALC
 */
public class TestVehiculoDAO {
    private Conexion conexion;
    @Mock
    private Connection mockConnection;
    @Mock
    private PreparedStatement mockPreparedStatement;
    @Mock
    private ResultSet rs;
    @Mock
    private MockResultTest mrs;
    private VehiculoDAO vehiculo;
    @Mock
    List<Vehiculo> datos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        conexion = Mockito.mock(Conexion.class, Mockito.RETURNS_DEEP_STUBS);
        vehiculo = Mockito.spy(new VehiculoDAO(conexion));
        mockConnection = Mockito.mock(Connection.class, Mockito.RETURNS_DEEP_STUBS);
        rs = Mockito.mock(ResultSet.class, Mockito.RETURNS_DEEP_STUBS);
        datos = Mockito.mock(ArrayList.class);
    }
    /**
     * El presente escenario representa el flujo normal de agregar un vehiculo
     */
    @Test
    public void agregarVehiculNormalTest() throws Exception {
        Vehiculo v = new Vehiculo();
        v.setPlaca("test");
        v.setMarca("test");
        v.setColor("test");
        v.setModelo("test");
        v.setAño(2021);
        v.setChevy(1);
        v.setCorreo("test");

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "insert into vehiculo(placa,marca,color,modelo,año,chevy,correo) values (?,?,?,?,?,?,?)";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.agregar(v));

    }
/**
     * El presente escenario representa cuando se va a registrar un vehiculo y el anio es negativo
     */
    @Test(expected = VehiculoException.class)
    public void agregarVehiculoAnoNegativoTest() throws Exception {
        Vehiculo v = new Vehiculo();
        v.setPlaca("test");
        v.setMarca("test");
        v.setColor("test");
        v.setModelo("test");
        v.setAño(-2021);
        v.setChevy(1);
        v.setCorreo("test");

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "insert into vehiculo(placa,marca,color,modelo,año,chevy,correo) values (?,?,?,?,?,?,?)";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.agregar(v));

    }
/**
     * El presente escenario representa el flujo normal de bsucar un vehiculo
     */
    @Test
    public void buscarVehiculoNormalTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.buscar("test"));

    }
/**
     * El presente escenario representa cuando se va a buscar un vehiculo pero no se ingresa la placa
     */
    @Test(expected = VehiculoException.class)
    public void buscarVehiculoNullPlacaTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.buscar(""));

    }
    /**
     * El presente escenario representa el flujo normal de actualizar un vehiculo
     */
    @Test
    public void actualizarVehiculNormalTest() throws Exception {
        Vehiculo v = new Vehiculo();
        v.setPlaca("test");
        v.setMarca("test");
        v.setColor("test");
        v.setModelo("test");
        v.setAño(2021);
        v.setChevy(1);
        v.setCorreo("test");

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "update vehiculo set placa=?, marca=?, modelo=?, color=?, año=?, chevy=?, correo=? where carro_id=?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.actualizar(v));

    }
    /**
     * El presente escenario representa cuando se va a actualizar un vehiculo pero no se ingresa la placa
     */
    @Test
    public void actualizarVehiculNullTest() throws Exception {
        Vehiculo v = new Vehiculo();
        v.setPlaca("test");
        v.setMarca("test");
        v.setColor("test");
        v.setModelo("test");
        v.setAño(2021);
        v.setChevy(1);
        v.setCorreo("test");

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "update vehiculo set placa=?, marca=?, modelo=?, color=?, año=?, chevy=?, correo=? where carro_id=?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.actualizar(null));

    }
    /**
     * El presente escenario representa el flujo normal de verificar un vehiculo
     */
    @Test
    public void verificarVehiculoNormalTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "SELECT placa FROM vehiculo WHERE placa = '" + "test" + "'";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.verificarVehiculo("test"));

    }
 /**
     * El presente escenario representa cuando se va a verificar un vehiculo pero no se ingresa la placa
     */
    @Test(expected = VehiculoException.class)
    public void verificarVehiculoNullPlacaTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "SELECT placa FROM vehiculo WHERE placa = '" + ""+ "'";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Respuesta  Mantenmiento" + vehiculo.verificarVehiculo(""));

    }
    /**
     * El presente escenario representa el flujo normal de realizar la consulta diaria de un vehiculo en mantenimiento
     */
        @Test
    public void consultarDiarioTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "select marca, modelo, año, color from vehiculo v INNER JOIN mantenimiento m ON v.carro_id = m.carro_id where fecha=?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Consulta exitosa" + vehiculo.consultar_diario("11-11-2021"));

    }
     /**
     * El presente escenario representa cuando se va a realizar la consulta diaria de un vehiculo en mantenimiento 
     * pero no se ingresa la fecha
     */
    
         @Test(expected = VehiculoException.class)
    public void consultarDiarioFechaNUllTest() throws Exception {

        Mockito.when(vehiculo.getConn()).thenReturn(mockConnection);
        String sql = "select marca, modelo, año, color from vehiculo v INNER JOIN mantenimiento m ON v.carro_id = m.carro_id where fecha=?";
        when(mockConnection.prepareStatement(sql)).thenReturn(mockPreparedStatement);
//        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(rs);
        System.out.println("Debe ingresar la fecha" + vehiculo.consultar_diario(""));

    }

}

    

