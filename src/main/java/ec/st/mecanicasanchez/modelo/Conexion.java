package ec.st.mecanicasanchez.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Santiago Tuqueres
 */
public class Conexion {

    private static final Logger LOG = Logger.getLogger(Conexion.class.getName());
    Connection con;
    String server = "jdbc:mysql://localhost:3306/";
    String bd = "mecanica";
    String lenguaje = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public Connection getConnection() {
        String url = server + bd + lenguaje;
        String user = "tuqui";
        String passw = "tuqui";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, passw);
        } catch (ClassNotFoundException | SQLException e) {
            LOG.log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, "El servidor de la Base de Datos no esta Activado",
                    "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
        }
        return con;
    }
}
