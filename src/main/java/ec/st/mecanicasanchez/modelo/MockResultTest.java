/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.st.mecanicasanchez.modelo;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.mockito.Mockito;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Santiago Tuqueres
 */
public class MockResultTest {

    private final Map<String, Integer> columnIndices;
    private final Object[][] data;
    private int rowIndex;

    private MockResultTest(final String[] columnNames,
            final Object[][] data) {
        this.columnIndices = IntStream.range(0, columnNames.length)
                .boxed()
                .collect(Collectors.toMap(
                        k -> columnNames[k],
                        Function.identity(),
                        (a, b)
                        -> {
                    throw new RuntimeException("Duplicate Column " + a);
                },
                        LinkedHashMap::new
                ));
        this.data = data;
        this.rowIndex = -1;
    }

    private ResultSet buildMock() throws SQLException {
        final ResultSet rs = mock(ResultSet.class);

        //MOCK rs.next()
        doAnswer(invocation -> {
            rowIndex++;
            return rowIndex < data.length;
        }).when(rs).next();
        //mock.rs.getString(columname)
        doAnswer(invocation -> {
            final String columnName = invocation.getArgument(0);
            final Integer columnIndex = columnIndices.get(columnName);
            return (String) data[rowIndex][columnIndex];

        }).when(rs).getString(Mockito.anyString());
        //mock rs.getOcject(columnIndex)
        doAnswer(invocation -> {
            final Integer index = invocation.getArgument(0);

            return data[rowIndex][index - 1];

        }).when(rs).getObject(Mockito.anyInt());

        final ResultSetMetaData rsmd = mock(ResultSetMetaData.class);

        doReturn(columnIndices.size()).when(rsmd).getColumnCount();

        doReturn(rsmd).when(rs).getMetaData();

        return rs;

    }

    public static ResultSet create(
            final String[] columnNames,
            final Object[][] data
    ) throws SQLException {
        return new MockResultTest(columnNames, data).buildMock();
    }

}
