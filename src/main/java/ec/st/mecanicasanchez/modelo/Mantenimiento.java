
package ec.st.mecanicasanchez.modelo;

/**
 *
 * @author Santiago Tuqueres
 */
public class Mantenimiento {
    int mantenimiento_id;
    int carro_id;
    String fecha_revision;;
    int kilometraje;
    String servicio;
    String observaciones;

    public Mantenimiento() {
    }

    public Mantenimiento(int mantenimiento_id,String fecha, int carro_id, int kilometraje, String servicio, String observaciones) {
        this.mantenimiento_id = mantenimiento_id;
        this.carro_id = carro_id; 
        this.fecha_revision=fecha;
        this.kilometraje = kilometraje;
        this.servicio = servicio;
        this.observaciones = observaciones;
    }

    public int getMantenimiento_id() {
        return mantenimiento_id;
    }

    public void setMantenimiento_id(int mantenimiento_id) {
        this.mantenimiento_id = mantenimiento_id;
    }

    public int getCarro_id() {
        return carro_id;
    }

    public void setCarro_id(int carro_id) {
        this.carro_id = carro_id;
    }
    public int getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(int kilometraje) {
        this.kilometraje = kilometraje;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public String getFecha_revision() {
        return fecha_revision;
    }

    public void setFecha_revision(String fecha_revision) {
        this.fecha_revision = fecha_revision;
    }
    
}
