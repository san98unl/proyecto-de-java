package ec.st.mecanicasanchez.modelo;

/**
 *
 * @author SANTIAGO 1998
 */
public class Cliente {
    int carro_id;
    int cliente_id;
    String nombre;
    int celular;
    public Cliente() {
    }
    public int getCarro_id() {
        return carro_id;
    }
    public void setCarro_id(int carro_id) {
        this.carro_id = carro_id;
    }
    public Cliente(String nombre, int celular) {
        this.nombre = nombre;
        this.celular = celular;
    }
    public int getCliente_id() {
        return cliente_id;
    }
    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getCelular() {
        return celular;
    }
    public void setCelular(int celular) {
        this.celular = celular;
    }
}
