package ec.st.mecanicasanchez.modelo;

import com.st.mecanicasanchez.vista.Vista;
import exepciones.VehiculoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Santiago Tuqueres
 */
public class VehiculoDAO {

    private static final Logger LOG = Logger.getLogger(Conexion.class.getName());
    Conexion conectar;
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Vista vista = new Vista();

    public VehiculoDAO() {
    }

    public VehiculoDAO(Conexion conexion) {
        this.conectar = conexion;
        con = conexion.getConnection();    }

    public Connection getConn() {
        return con;
    }

    public void setConn(Connection conn) {
        this.con = conn;
    }

    public List listar() {//no usado
        List<Vehiculo> datos = new ArrayList<>();
        String sql = "select * from vehiculo";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vehiculo carro = new Vehiculo();

                carro.setPlaca(rs.getString(2));
                carro.setMarca(rs.getString(3));
                carro.setModelo(rs.getString(4));
                carro.setColor(rs.getString(5));
                carro.setAño(rs.getInt(6));
                carro.setChevy(rs.getInt(7));
                carro.setCorreo(rs.getString(8));
                carro.setCarroid(rs.getInt(1));

                datos.add(carro);

            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return datos;
    }

    public int agregar(Vehiculo c) throws VehiculoException {
        String sql = "insert into vehiculo(placa,marca,color,modelo,año,chevy,correo) values (?,?,?,?,?,?,?)";
        if (c.getAño() < 0) {
            throw new VehiculoException("Vehiculo ano Invalido");
        }
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, c.getPlaca());
            ps.setString(2, c.getMarca());
            ps.setString(3, c.getColor());
            ps.setString(4, c.getModelo());
            ps.setInt(5, c.getAño());
            ps.setInt(6, c.getChevy());
            ps.setString(7, c.getCorreo());

            ps.executeUpdate();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return 1;
    }

    public int actualizar(Vehiculo c) {
        String sql = "update vehiculo set placa=?, marca=?, modelo=?, color=?, año=?, chevy=?, correo=? where carro_id=?";
        int r = 0;
        try {

            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            //
            ps.setString(1, c.getPlaca());
            ps.setString(2, c.getMarca());
            ps.setString(3, c.getModelo());
            ps.setString(4, c.getColor());
            ps.setInt(5, c.getAño());
            ps.setInt(6, c.getChevy());
            ps.setString(7, c.getCorreo());
            ps.setInt(8, c.getCarroid());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return r;
    }

    public List buscar(String placa) throws Exception, VehiculoException {
        List<Vehiculo> datos = new ArrayList<>();
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        if (placa == "" || placa == null) {
            throw new VehiculoException("Placa Invalida");
        }
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, placa + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Vehiculo carro = new Vehiculo();

                carro.setPlaca(rs.getString(1));
                carro.setMarca(rs.getString(2));
                carro.setModelo(rs.getString(3));
                carro.setColor(rs.getString(4));
                carro.setAño(rs.getInt(5));
                carro.setChevy(rs.getInt(6));
                carro.setCorreo(rs.getString(7));
                carro.setCarroid(rs.getInt(11));

                datos.add(carro);

            }
        } catch (SQLException e) {
//            LOG.log(Level.SEVERE, null, e);
//            JOptionPane.showMessageDialog(vista, "No se encontraron resultados");
        }
        return datos;
    }

    public boolean verificarVehiculo(String placa) throws Exception, VehiculoException {
        boolean res = false;
        String verificar = "SELECT placa FROM vehiculo WHERE placa = '" + placa + "'";
        System.out.println(verificar);
        if (placa == "" || placa == null) {
            throw new VehiculoException("Placa Invalida");
        }
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(verificar);
            rs = ps.executeQuery();
            if (rs.next()) {
                res = true;
            }
        } catch (Exception e) {
            System.err.print("Ha ocurrido un error: " + e.getMessage());
        } finally {
            ps = null;
            //con.close();
        }
        return res;
    }

    public List consultar_diario(String fecha) throws Exception, VehiculoException{//
        List<Vehiculo> datos = new ArrayList<>();
        String sql = "select marca, modelo, año, color from vehiculo v INNER JOIN mantenimiento m ON v.carro_id = m.carro_id where fecha=?";
        if(fecha == ""){
            throw new VehiculoException("Placa Invalida");
        }
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, fecha);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vehiculo carro = new Vehiculo();
                carro.setMarca(rs.getString(1));
                carro.setModelo(rs.getString(2));
                carro.setAño(rs.getInt(3));
                carro.setColor(rs.getString(4));
                datos.add(carro);

            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return datos;
    }
}
