package ec.st.mecanicasanchez.modelo;

/**
 *
 * @author Santiago Tuqueres
 */
public class Vehiculo {

    int carro_id;
    String placa;
    String marca;
    String color;
    String modelo;
    int año;
    int chevy;
    String correo;

    public Vehiculo() {
    }

    public Vehiculo(int Carroid, String placa, String marca, String color, int año, int kilometraje, String correo, String modelo) {
        this.carro_id = Carroid;
        this.placa = placa;
        this.marca = marca;
        this.color = color;
        this.año = año;
        this.chevy = kilometraje;
        this.correo = correo;
        this.modelo= modelo;
    }

    public int getCarro_id() {
        return carro_id;
    }

    public void setCarro_id(int carro_id) {
        this.carro_id = carro_id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCarroid() {
        return carro_id;
    }

    public void setCarroid(int Carroid) {
        this.carro_id = Carroid;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getChevy() {
        return chevy;
    }

    public void setChevy(int chevy) {
        this.chevy = chevy;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

}
