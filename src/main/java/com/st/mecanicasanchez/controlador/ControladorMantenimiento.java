package com.st.mecanicasanchez.controlador;

import com.st.mecanicasanchez.vista.Vista;
import com.st.mecanicasanchez.vista.VistaMantenimiento;
import com.st.mecanicasanchez.vista.VistaMantenimientoEliminar;
//import com.sun.tools.javac.util.ArrayUtils;
import ec.st.mecanicasanchez.modelo.Conexion;
import ec.st.mecanicasanchez.modelo.Vehiculo;
import ec.st.mecanicasanchez.modelo.Mantenimiento;
import ec.st.mecanicasanchez.modelo.MantenimientoDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Santiago Tuqueres
 */
public class ControladorMantenimiento implements ActionListener {

    private static final Logger LOG = Logger.getLogger(ControladorMantenimiento.class.getName());
    MantenimientoDAO dao = new MantenimientoDAO();
    Mantenimiento mantenimiento = new Mantenimiento();
    VistaMantenimiento vista = new VistaMantenimiento();
    VistaMantenimientoEliminar vista2 = new VistaMantenimientoEliminar();

    JList jl1 = vista.jl_service1;
    JList jl2 = vista.jl_servicio2;
    JList jl3 = vista.jl_servicio3;
    JList jl4 = vista.jl_servicio4;

    int selected[];

    public ControladorMantenimiento(VistaMantenimiento v) {
        this.vista = v;
        this.vista.btn_buscar.addActionListener(this);
        this.vista.btn_guardar.addActionListener(this);
    }
    DefaultTableModel modelo = new DefaultTableModel();

    public ControladorMantenimiento(VistaMantenimientoEliminar v) {
        this.vista2 = v;
        this.vista2.btn_buscar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public void agregar(int kilometraje, int vehiculo_id, String servicios, String observaciones) throws Exception {
        try {
            mantenimiento.setCarro_id(vehiculo_id);
            mantenimiento.setKilometraje(kilometraje);
            mantenimiento.setObservaciones(observaciones);
            mantenimiento.setServicio(servicios);

            int r = dao.agregar(mantenimiento);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Mantenimiento Agregado con Exito");
                //limpiarTabla();
            } else {
                JOptionPane.showMessageDialog(vista, "Hubo un Error");
            }

        } catch (NumberFormatException ex) { // handle your exception
            LOG.log(Level.SEVERE, null, ex);
        }
    }
//busca vehiculos

    public void listar_buscar(JTable tabla, String criterio) throws Exception {
        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        List<Vehiculo> lista = dao.buscar(criterio);
        Object[] object = new Object[7];
        for (int i = 0; i < lista.size(); i++) {
            object[0] = lista.get(i).getCarroid();
            object[1] = lista.get(i).getPlaca();
            object[2] = lista.get(i).getMarca();
            object[3] = lista.get(i).getColor();
            object[4] = lista.get(i).getAño();
            object[5] = lista.get(i).getChevy();
            object[6] = lista.get(i).getCorreo();
            modelo.addRow(object);
        }

        if (lista.isEmpty()) {
            //JOptionPane.showMessageDialog(vista, "No se ha encontrado resultados en la Busqueda");
            //crear accion para registrar el vehiculo
            //prueba
            int seleccion = JOptionPane.showOptionDialog(
                    null,
                    "No se han encintrado resultados en la busqueda",
                    "Selector de opciones",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null, // null para icono por defecto.
                    new Object[]{"Registrar Carro", "Cancelar"}, // null para YES, NO y CANCEL
                    "opcion 1");

            if (seleccion == 0) {
                //se necesita enlazar la accion
                Vista v = new Vista();
                v.setVisible(true);

            }
        } else {
            //JOptionPane.showMessageDialog(vista, "Busqueda Exitosa");
        }
        vista.tabla.setModel(modelo);
    }
//busca mantenimientos

    public void listar_buscar_mantenimiento(JTable tabla, String criterio) throws Exception{//EN PRUEBA
        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        List<Mantenimiento> lista = dao.buscar_mantenimiento(criterio);
        Object[] object = new Object[6];

        for (int i = 0; i < lista.size(); i++) {
            object[0] = lista.get(i).getMantenimiento_id();
            object[1] = lista.get(i).getFecha_revision();
            object[2] = lista.get(i).getKilometraje();
            object[3] = lista.get(i).getServicio();
            object[4] = lista.get(i).getObservaciones();
            //object[5] = lista.get(i)
            modelo.addRow(object);

        }
        if (object != null) {
            // JOptionPane.showMessageDialog(vista, "Busqueda Exitosa");
        } else {
            JOptionPane.showMessageDialog(vista, "No se ha encontrado resultados en la Busqueda");
            //poner accion de registrar vehiculo
        }

        vista.tabla.setModel(modelo);
    }

    public void eliminar(int id) {
        dao.eliminar(id);
    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    public void limpiarTabla(JTable tabla) {
        for (int i = 0; i < tabla.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
}
