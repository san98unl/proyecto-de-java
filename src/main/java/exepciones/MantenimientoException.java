/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package exepciones;

/**
 *
 * @author Santiago Tuqueres
 */
public class MantenimientoException extends Exception{

    /**
     * Creates a new instance of <code>MantenimientoException</code> without
     * detail message.
     */
    public MantenimientoException() {
    }

    /**
     * Constructs an instance of <code>MantenimientoException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public MantenimientoException(String msg) {
        super(msg);
    }
}
