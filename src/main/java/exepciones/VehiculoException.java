/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exepciones;

/**
 *
 * @author EALC
 */
public class VehiculoException extends Exception {

    /**
     * Creates a new instance of <code>VehiculoException</code> without detail
     * message.
     */
    public VehiculoException() {
    }

    /**
     * Constructs an instance of <code>VehiculoException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public VehiculoException(String msg) {
        super(msg);
    }
}
